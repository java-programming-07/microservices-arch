# Business Account Microservice

- The business account microservice is according to the environment:

> curl --location --request GET 'http://localhost:{gateway_port}/account/96721'

### Actuator

http://localhost:{random}/actuator/health

- Documentation: https://www.baeldung.com/spring-boot-actuators
