package com.jevillac07.businessaccount.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AccountResponse {
  private String cic;

  private String dni;

  private String name;

  private String lastName;
}
