package com.jevillac07.businessaccount.service;

import com.jevillac07.businessaccount.model.AccountResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class AccountServiceImp implements AccountService {
  @Override
  public AccountResponse getAccountByCic(String cic) {
    return getAccounts()
      .stream()
      .filter(account -> Objects.equals(account.getCic(), cic))
      .findFirst()
      .orElse(AccountResponse.builder().build())
      ;
  }

  private List<AccountResponse> getAccounts() {
    AccountResponse accountsOne = AccountResponse.builder()
      .cic("96720")
      .dni("49045621")
      .name("John")
      .lastName("Doe")
      .build();

    AccountResponse accountsTwo = AccountResponse.builder()
      .cic("96721")
      .dni("46231845")
      .name("Edward")
      .lastName("Jones")
      .build();

    AccountResponse accountsThree = AccountResponse.builder()
      .cic("96722")
      .dni("73482310")
      .name("Jacob")
      .lastName("Williams")
      .build();

    return List.of(accountsOne, accountsTwo, accountsThree);
  }
}
