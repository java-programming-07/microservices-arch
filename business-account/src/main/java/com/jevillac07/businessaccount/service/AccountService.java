package com.jevillac07.businessaccount.service;

import com.jevillac07.businessaccount.model.AccountResponse;

public interface AccountService {
  AccountResponse getAccountByCic(String cic);
}
