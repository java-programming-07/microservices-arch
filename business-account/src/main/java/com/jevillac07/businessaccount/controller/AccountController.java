package com.jevillac07.businessaccount.controller;

import com.jevillac07.businessaccount.model.AccountResponse;
import com.jevillac07.businessaccount.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class AccountController {
  private AccountService accountService;

  @GetMapping("/account/{cic}")
  public ResponseEntity<AccountResponse> getAccount(@PathVariable String cic) {
    return ResponseEntity.ok(
      accountService.getAccountByCic(cic));
  }
}
