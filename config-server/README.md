# Config Server
The configuration is in the next line in application.yaml file:
> search-paths: 'config-data/{profile}'

* The **config-data** directory must exist in the root repository.
* The **dev** and **cer** directories must be created within the **config-data** directory.

#### Structure:
* root
  * config-data
    * dev
    * cer

### Config Server API
> curl --location 'http://localhost:8089/card-product/dev'
>
> curl --location 'http://localhost:8089/business-account/dev'
