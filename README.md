# Microservices Architecture

Run the applications in the following order:

- Config server (port: 8089) 
- Eureka service (port: dev - 8761 | cer - 8762)
- Gateway (port: dev - 8000 | cer: 7000)
- Microservices:
  1. Business account (port: {gateway})
  2. Card product (port: {gateway})
