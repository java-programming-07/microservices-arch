# Card Product Microservice

- The card product microservice is according to the environment:

> curl --location --request GET 'http://localhost:{gateway_port}/card-product/96721'

### How to assign a random port?

Edit Configuration > Modify options > Allow multiple instances

![img.png](img/01_img.png)

Then press the play button

![img.png](img/02_img.png)

### Actuator

- GET :
> http://localhost:{random}/actuator/health

- POST : refresh the scope in CardProductConfig class
> http://localhost:{random}/actuator/refresh

- Documentation: https://www.baeldung.com/spring-boot-actuators

### Hashicorp Vault
1. Download hashicorp vault: https://developer.hashicorp.com/vault/install?product_intent=vault
2. Run hashicorp vault from the command prompt with the following line:
```sh
$ vault server --dev --dev-root-token-id="00000000-0000-0000-0000-000000000000"
```
3. Open the browser with the following url: http://127.0.0.1:8200. You must enter the token 00000000-0000-0000-0000-000000000000.
4. Go to **secret/** option and then create a new secret like the following image

![img.png](img/03_img.png)

```sh
# json code:
{
  "response.display-name": "Saving Account"
}
```
