package com.jevillac07.cardproduct.controller;

import com.jevillac07.cardproduct.model.CardProductResponse;
import com.jevillac07.cardproduct.service.CardProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CardProduct {
  private CardProductService cardProductService;

  @GetMapping("/card-product/{cic}")
  public ResponseEntity<CardProductResponse> getCardProduct(@PathVariable String cic) {
    return ResponseEntity.ok(
      cardProductService.getCardProductByCic(cic));
  }
}
