package com.jevillac07.cardproduct.service;

import com.jevillac07.cardproduct.configuration.CardProductConfig;
import com.jevillac07.cardproduct.configuration.HashicorpVaultConfig;
import com.jevillac07.cardproduct.feign.BusinessAccountFeign;
import com.jevillac07.cardproduct.model.AccountResponseFeign;
import com.jevillac07.cardproduct.model.CardProductResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CardProductServiceImp implements CardProductService {
  private BusinessAccountFeign businessAccountFeign;
  private HashicorpVaultConfig hashicorpVaultConfig;
  private CardProductConfig cardProductConfig;

  @Override
  public CardProductResponse getCardProductByCic(String cic) {
    return CardProductResponse.builder()
      .cic(cic)
      .productHolder(getAccountResponse(cic))
      .displayName(hashicorpVaultConfig.getDisplayName())
      .amount(cardProductConfig.getAmount())
      .build();
  }

  private String getAccountResponse(String cic) {
    AccountResponseFeign accountResponseFeign = businessAccountFeign.getAccount(cic);
    return accountResponseFeign.getCic() != null
      ? accountResponseFeign.getName() + " " + accountResponseFeign.getLastName()
      : "Account without data";
  }
}
