package com.jevillac07.cardproduct.service;

import com.jevillac07.cardproduct.model.CardProductResponse;

public interface CardProductService {

    CardProductResponse getCardProductByCic(String cic);
}
