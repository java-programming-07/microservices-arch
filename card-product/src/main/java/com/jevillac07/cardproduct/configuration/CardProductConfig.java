package com.jevillac07.cardproduct.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Getter
@Setter
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "card-product.saving-account")
public class CardProductConfig {
  private BigDecimal amount;
}
