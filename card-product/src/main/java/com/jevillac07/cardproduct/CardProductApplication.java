package com.jevillac07.cardproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CardProductApplication {

  public static void main(String[] args) {
    SpringApplication.run(CardProductApplication.class, args);
  }

}
