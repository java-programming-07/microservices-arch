package com.jevillac07.cardproduct.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AccountResponseFeign {
  private String cic;

  private String dni;

  private String name;

  private String lastName;
}
