package com.jevillac07.cardproduct.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class CardProductResponse {
  private String cic;

  private String displayName;

  private String productHolder;

  private BigDecimal amount;
}
