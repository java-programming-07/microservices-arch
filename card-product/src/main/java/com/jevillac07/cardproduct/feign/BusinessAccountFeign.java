package com.jevillac07.cardproduct.feign;

import com.jevillac07.cardproduct.model.AccountResponseFeign;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "business-account", url = "${environment.business-account-uri}")
public interface BusinessAccountFeign {
  @GetMapping("/account/{cic}")
  AccountResponseFeign getAccount(@PathVariable String cic);
}
